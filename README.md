1. This is a sample program using tdd as development strategy, it will make a lifecounter for magic trading card games.

This document will expose the requirements for the software, that will be fullfilled using :

Eclipse Mars IDE
	The latest (so far) version of the Eclipse IDE
Junit 4
	The x-unit framework for java, yet simple it allows to isolate test with simple assertions wether a result has the expected value
	
Mockito 
	it is a mock framework used to create fake objects from the declared classes.

eclEmma pluggin for eclipse
	A code coverage tool that shows the percentage of the production code covered by the tests.


