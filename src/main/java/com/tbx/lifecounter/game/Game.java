package com.tbx.lifecounter.game;

import java.util.ArrayList;
import java.util.List;

import com.tbx.lifecounter.player.Player;

public class Game {

	List<Player> playerList;
	
	public Game(){
		playerList = new ArrayList<Player>();
	}
	
	public void addPlayer(Player newPlayer) {
		playerList.add(newPlayer);
		
	}
	
	public int addedPlayers(){
		
		return playerList.size();
		
	}

}
