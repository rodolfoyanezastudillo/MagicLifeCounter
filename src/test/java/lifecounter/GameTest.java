package lifecounter;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.tbx.lifecounter.game.Game;
import com.tbx.lifecounter.player.Player;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

	Game game;
	
	@Before
	public void setup(){
		game = new Game();
	}
	
	@Test
	public void addPlayer_addFirsPlayerWithNoPreviusPlayers_addedPlayerReturnsOne() {
		
		Player newPlayer = mock(Player.class);
		game.addPlayer(newPlayer);
		assertEquals(1,game.addedPlayers());
		
	}

}
